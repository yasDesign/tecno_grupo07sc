/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo07sc;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import negocio.NClientes;
import negocio.NDetalles;
import negocio.NDevoluciones;
import negocio.NEstadisticas;
import negocio.NPedidos;
import negocio.NProductos;
import negocio.NReportes;
import negocio.NTipoProductos;
import negocio.NVendedor;
import negocio.NVentas;

/**
 *
 * @author tecno
 */
public class Telnet {

    private int max = 0;
    private PopMessage m_PopMessage2;
    private SMTPMessage m_SMTPMessage2;
    
    private String g_metodo="";
    private String g_origen="";   

    public Telnet() {
        m_PopMessage2 = new PopMessage();
        max = m_PopMessage2.getSize();
        m_PopMessage2.cerrar();
    }

    public void leer() {
        m_PopMessage2 = new PopMessage();
        if (m_PopMessage2.getSize() > max) {
            max++;
            //analizarLineas(m_PopMessage2.getMessageArray(max));
            boolean estado=analizarLineasSi(m_PopMessage2.getMessageArray(max));
            if (estado) {
                enviar_respuesta_(true);
            }else{
                enviar_respuesta_(false);
            }
        }
        m_PopMessage2.cerrar();
    }

    public void analizarLineas(List<String> messageArray) {
        String lineaMetodo = "";
        String lineaUsuario = "";
        int i=0;
        for (String line : messageArray) {
            //System.out.println(line.toString());
            if (line.contains("Return-Path:")) {
              lineaUsuario = line;
            }
            if (line.contains("Subject:")||line.contains("subject:")) {
                if (line.regionMatches(0, "Subject:", 0,8 )||line.regionMatches(0, "subject:", 0,8 )) {
                    while(!messageArray.get(i).contains(")")){
                        lineaMetodo=lineaMetodo+messageArray.get(i);
                        i++;
                    }
                    lineaMetodo=lineaMetodo+messageArray.get(i);
                }                
            }
            i++;
            }
        System.out.println("linea encontrada=>"+lineaMetodo);
            //i++;
        
        //obtener mail usuario
        String mailFrom = getCorreoEmisor(lineaUsuario);
        System.out.println(mailFrom);
        
            //obtener metodo
            //posisiones para metodo y parametros
            int posIni = lineaMetodo.indexOf("(");
            int posFin = lineaMetodo.indexOf(")");
            String metodo = getMetodo(lineaMetodo, posIni);
            System.out.println("metodo-"+metodo);
            //obtener parametros        
            String[] parametros = getParametros(lineaMetodo, posIni, posFin);
            System.out.println(parametros.toString());
            ejecutarMetodos(metodo, parametros, mailFrom);
        

    }

    private String getMetodo(String lineaMetodo, int posIni) {
        //obtener metodo
        String metodo = lineaMetodo.substring(8, posIni).trim();
        metodo=metodo.toUpperCase();
        if (metodo.length() == 0) {
            metodo = "AYUDA";
        }
        return metodo;
    }

    private String[] getParametros(String lineaMetodo, int posIni, int posFin) {
        String[] parametros = lineaMetodo.substring(posIni + 1, posFin).split(",");
        return parametros;
    }

    private String getCorreoEmisor(String lineaUsuario) {
        //posiciones para usuario mail
        int posIni1 = lineaUsuario.indexOf("<");
        int posFin1 = lineaUsuario.indexOf(">");
        return lineaUsuario.substring(posIni1 + 1, posFin1);
    }
      private void enviarMensajeCorreoOrigen(String prt_mailFrom, String prt_asunto, String prt_mensaje) {
        m_SMTPMessage2 = new SMTPMessage();
        m_SMTPMessage2.sendMessage("grupo07sc@mail.ficct.uagrm.edu.bo", prt_mailFrom, prt_asunto, prt_mensaje);
        m_SMTPMessage2.cerrar();
    }

    private boolean analizarLineasSi(List<String> messageArray) {
        g_origen="";
        g_metodo="";        
        
        String lineaMetodo = "";
        String lineaUsuario = "";
        int i=0;
        for (String line : messageArray) {
            //System.out.println(line.toString());
            if (line.contains("Return-Path:")) {
              lineaUsuario = line;
              //guardar linea correo origen
              g_origen=lineaUsuario;              
            }
            if (line.contains("Subject:")||line.contains("subject:")) {
                if (line.regionMatches(0, "Subject:", 0,8 )||line.regionMatches(0, "subject:", 0,8 )) {
                    while(!messageArray.get(i).contains(")")){
                        lineaMetodo=lineaMetodo+messageArray.get(i);
                        i++;
                    }
                    
                    lineaMetodo=lineaMetodo+messageArray.get(i);
                    //guardar linea metodo globa;
                    
                    g_metodo=lineaMetodo;
                    return true;
                }                
            }
            i++;
            }
        return false;
    }

    private void enviar_respuesta_(boolean b) {
        if (b) {
            String mailFrom = getCorreoEmisor(g_origen);
            
            int posIni = g_metodo.indexOf("(");
            int posFin = g_metodo.indexOf(")");
            String metodo = getMetodo(g_metodo, posIni);
            
            String[] parametros = getParametros(g_metodo, posIni, posFin);
            
            ejecutarMetodos(metodo, parametros, mailFrom);
        }else{
            System.out.println("lo siento no se pudo mandar no se encontro el metodo.. \r\n");
        }
    }

    private void ejecutarMetodos(String prt_asunto, String[] prt_parametros, String prt_mailFrom) {
        String mensaje = "";
        //
        switch (prt_asunto) {
            
            // GESTIONAR VENDEDORES
            case "REGISTRARVENDEDOR":
                System.out.println(prt_asunto + "...\r\n");
                NVendedor m_NVendedor=new NVendedor();
                String res=m_NVendedor.registrar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom,prt_asunto,getMensajeRespuesta(res));
                break;            
            case "EDITARVENDEDOR":
                System.out.println(prt_asunto + "...\r\n");
                NVendedor m_NVendedor2=new NVendedor();
                String res2=m_NVendedor2.editar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res2));
                break;    
            case "ELIMINARVENDEDOR":
                System.out.println(prt_asunto + "...\r\n");
                NVendedor m_NVendedor3=new NVendedor();
                String res3=m_NVendedor3.eliminar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res3));
                break;    
            case "LISTARVENDEDORES":
                System.out.println(prt_asunto + "...\r\n");
                NVendedor m_NVendedor4=new NVendedor();
                String res4=m_NVendedor4.listar();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res4));
                break; 
            
            // GESTIONAR CLIENTES
            case "REGISTRARCLIENTE":
                System.out.println(prt_asunto + "...\r\n");
                NClientes m_NClientes=new NClientes();
                String res_cliente=m_NClientes.registrar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res_cliente));
                break;            
            case "EDITARCLIENTE":
                System.out.println(prt_asunto + "...\r\n");
                NClientes m_NClientes2=new NClientes();
                String res2_cliente=m_NClientes2.editar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res2_cliente));
                break;    
            case "ELIMINARCLIENTE":
                System.out.println(prt_asunto + "...\r\n");
                NClientes m_NClientes3=new NClientes();
                String res3_cliente=m_NClientes3.eliminar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res3_cliente));
                break;    
            case "LISTARCLIENTES":
                System.out.println(prt_asunto + "...\r\n");
                NClientes m_NClientes4=new NClientes();
                String res4_cliente=m_NClientes4.listar();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res4_cliente));
                break;  
                
             // GESTIONAR PRODUCTOS
            case "REGISTRARPRODUCTO":
                System.out.println(prt_asunto + "...\r\n");
                NProductos m_NProductos=new NProductos();
                String res_pro=m_NProductos.registrar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom,prt_asunto, getMensajeRespuesta(res_pro));
                break;            
            case "EDITARPRODUCTO":
                System.out.println(prt_asunto + "...\r\n");
                NProductos m_NProductos2=new NProductos();
                String res_pro2=m_NProductos2.editar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res_pro2));
                break;    
            case "ELIMINARPRODUCTO":
                System.out.println(prt_asunto + "...\r\n");
                NProductos m_NProductos3=new NProductos();
                String res_pro3=m_NProductos3.eliminar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res_pro3));
                break;    
            case "LISTARPRODUCTOS":
                System.out.println(prt_asunto + "...\r\n");
                NProductos m_NProductos4=new NProductos();
                String res_pro4=m_NProductos4.listar();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res_pro4));
                break;    
                
            // GESTIONAR TIPO PRODUCTOS
            case "REGISTRARTIPOPRODUCTO":
                System.out.println(prt_asunto + "...\r\n");
                NTipoProductos m_NTipoProductos=new NTipoProductos();
                String res_tipopro=m_NTipoProductos.registrar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom,prt_asunto, getMensajeRespuesta(res_tipopro));
                break;            
            case "EDITARTIPOPRODUCTO":
                System.out.println(prt_asunto + "...\r\n");
                NTipoProductos m_NTipoProductos2=new NTipoProductos();
                String res_tipopro2=m_NTipoProductos2.editar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res_tipopro2));
                break;    
            case "ELIMINARTIPOPRODUCTO":
                System.out.println(prt_asunto + "...\r\n");
                NTipoProductos m_NTipoProductos3=new NTipoProductos();
                String res_tipopro3=m_NTipoProductos3.eliminar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res_tipopro3));
                break;    
            case "LISTARTIPOPRODUCTOS":
                System.out.println(prt_asunto + "...\r\n");
                NTipoProductos m_NTipoProductos4=new NTipoProductos();
                String res_tipopro4=m_NTipoProductos4.listar();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res_tipopro4));
                break;
            
                // GESTIONAR PEDIDOS
            case "CREARPEDIDO":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos=new NPedidos();
                String res_ped=m_NPedidos.crear(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom,prt_asunto, getMensajeRespuesta(res_ped));
                break;            
            case "CREARPEDIDOLOCAL":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos1=new NPedidos();
                String res1_ped=m_NPedidos1.crearLocal(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res1_ped));
                break;    
            case "FINALIZARPEDIDO":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos2=new NPedidos();
                String res2_ped=m_NPedidos2.finalizar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res2_ped));
                break;    
            case "ENTREGARPEDIDO":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos3=new NPedidos();
                String res3_ped=m_NPedidos3.entregar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res3_ped));
                break;
            case "REGISTRARVENTA":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos4=new NPedidos();
                String res4_ped=m_NPedidos4.registrar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res4_ped));
                break;            
            case "CANCELARPEDIDO":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos5=new NPedidos();
                String res5_ped=m_NPedidos5.cancelar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res5_ped));
                break;    
            case "DEVOLVERVENTA":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos6=new NPedidos();
                String res6_ped=m_NPedidos6.devolucion(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res6_ped));
                break;    
            case "LISTARPEDIDOS":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos7=new NPedidos();
                String res7_ped=m_NPedidos7.listar();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res7_ped));
                break;
            case "VERPEDIDO":
                System.out.println(prt_asunto + "...\r\n");
                NPedidos m_NPedidos8=new NPedidos();
                String res8_ped=m_NPedidos8.ver(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res8_ped));
                break;
                
                // GESTIONAR PEDIDOS
            case "AGREGARALCARRITO":
                System.out.println(prt_asunto + "...\r\n");
                NDetalles m_NDetalle=new NDetalles();
                String res_det=m_NDetalle.registrar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res_det));
                break;      
            case "ElIMINARDELCARRITO":
                System.out.println(prt_asunto + "...\r\n");
                NDetalles m_NDetalle1=new NDetalles();
                String res1_det=m_NDetalle1.eliminar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res1_det));
                break; 
            case "EDITARDELCARRITO":
                System.out.println(prt_asunto + "...\r\n");
                NDetalles m_NDetalle2=new NDetalles();
                String res2_det=m_NDetalle2.editar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res2_det));
                break; 
            case "VERCARRITO":
                System.out.println(prt_asunto + "...\r\n");
                NDetalles m_NDetalle3=new NDetalles();
                String res3_det=m_NDetalle3.ver(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res3_det));
                break; 
                
                // GESTIONAR VENTAS
            case "ElIMINARVENTA":
                System.out.println(prt_asunto + "...\r\n");
                NVentas m_NVentas=new NVentas();
                String res1_ven=m_NVentas.eliminar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res1_ven));
                break;  
            case "LISTARVENTAS":
                NVentas m_NVentas2=new NVentas();
                String res2_ven=m_NVentas2.listar();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res2_ven));
                break; 
                    
                
                // GESTIONAR DEVOLUCIONES
            case "ELIMNARDEVOLUCION":
                System.out.println(prt_asunto + "...\r\n");
                NDevoluciones m_NDevoluciones=new NDevoluciones();
                String res1_dev=m_NDevoluciones.eliminar(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(res1_dev));
                break;  
            case "LISTARDEVOLUCIONES":
                System.out.println(prt_asunto + "...\r\n");
                NDevoluciones m_NDevoluciones2=new NDevoluciones();
                String res2_dev=m_NDevoluciones2.listar();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res2_dev));
                break;
                
                //REPORTES
            case "VERREPORTES":
                System.out.println(prt_asunto + "...\r\n");
                NReportes m_NReportes=new NReportes();
                String res_rep=m_NReportes.VerReporte();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res_rep));
                break;
                
                //REPORTES
            case "VERESTADISTICAS":
                System.out.println(prt_asunto + "...\r\n");
                NEstadisticas m_NEstadistica=new NEstadisticas();
                String res_estadistica=m_NEstadistica.verEstadisticas();
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeTabla(res_estadistica));
                break;
                
                // AYUDA
            case "AYUDA":
                System.out.println(prt_asunto + "...\r\n");
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeAyuda());
                break;            

            default:
                System.out.println(prt_asunto + " no existe ...\r\n");
                String mensaje5 = "error en la instruccion porfavor revisa  al enviado 'ayuda();' de la aplicacion";
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto, getMensajeRespuesta(mensaje5));
                break;
                
        }
    }
    
    //******************************//
    //***ESTILO PARA LA VISTA***/////
    //******************************//
    public String getMensajeAyuda(){
        
        String estilo="<link rel='stylesheet' href='https://codepen.io/ingyas/pen/wYYaqa.css'>";
        String titulo="<h2>AYUDA</h2>";
        String ayudaCliente="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>GESTIONAR CLIENTES</h3>" +
                                "<strong>Registrar Clientes:</strong><p>REGISTRARCLIENTE(nombre,imagen,correo,contrasena,celular,direccion,latitud,longitud);</p>" +
                                "<strong>Ejemplo:</strong><p><i>REGISTRARCLIENTE(juan perez,user_default.png,juanp@gmail.com,contrasena,2132312,av.los angel1es,12.123423,-12.234231);</i></p>"+
                                "<strong>Eliminar Clientes:</strong><p>ELIMINARCLIENTE(id);</p>" +
                                "<strong>Ejemplo:</strong><p><i>ELIMINARCLIENTE(3);</i></p>"+
                                "<strong>Editar Clientes:</strong><p>EDITARCLIENTE(id,nombre,imagen,correo,contrasena,celular,direccion,latitud,longitud);</p>" +
                                "<strong>Ejemplo:</strong><p><i>EDITARCLIENTE(2,juan perez,user_default.png,juanp@gmail.com,contrasena,2132312,av.los angel1es,12.123423,-12.234231);</i></p>"+
                                "<strong>Listar Clientes:</strong><p>LISTARCLIENTES();</p>" +
                            "</div>";
        
        String ayudaVendedor="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>GESTIONAR VENDEDORES</h3>" +
                                "<strong>Registrar Vendedores:</strong><p>REGISTRARVENDEDOR(nombre,imagen,correo,contrasena,celular,direccion,latitud,longitud);</p>" +
                                "<strong>Ejemplo:</strong><p><i>REGISTRARVENDEDOR(felipe suarez paniagua,user_default.png,felipe_suares@gmail.com,felipe123,231233,av moscu y 5to anillo,-56.2134,2.12312);</i></p>"+
                                "<strong>Eliminar Vendedores:</strong><p>ELIMINARVENDEDOR(id);</p>" +
                                "<strong>Ejemplo:</strong><p><i>ELIMINARVENDEDOR(4);</i></p>"+
                                "<strong>Editar Vendedores:</strong><p>EDITARVENDEDOR(id,nombre,imagen,correo,contrasena,celular,direccion,latitud,longitud);</p>" +
                                "<strong>Ejemplo:</strong><p><i>EDITARVENDEDOR(5,ana booleana asurduy,user_default.png,anita@gmail.com,ana123,231233,av santiestevan y av colorado nro23,-21.1134,23.2312);</i></p>"+
                                "<strong>Listar Vendedores:</strong><p>LISTARVENDEDORES();</p>" +
                            "</div>";
        
        String ayudaTipoProductos="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>GESTIONAR TIPO DE PRODUCTOS</h3>" +
                                "<strong>Registrar Tipo Productos:</strong><p>REGISTRARTIPOPRODUCTO(nombre,descripcion,imagen);</p>" +
                                "<strong>Eliminar Tipo Productos:</strong><p>ELIMINARTIPOPRODUCTO(idproducto);</p>" +
                                "<strong>Editar Tipo Productos:</strong><p>EDITARTIPOPRODUCTOS(id,nombre,descripcion,imagen);</p>" +
                                "<strong>Listar Tipo Productos:</strong><p>LISTARTIPOPRODUCTOS();</p>" +
                            "</div>";
        
        String ayudaProductos="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>GESTIONAR PRODUCTOS</h3>" +
                                "<strong>Registrar Productos:</strong><p>REGISTRARPRODUCTO(nombre,descripcion,precio,tamaño,imagen,idtipo);</p>" +
                                "<strong>Eliminar Productos:</strong><p>ELIMINARPRODUCTO(id);</p>" +
                                "<strong>Editar Productos:</strong><p>EDITARPRODUCTOS(id,nombre,descripcion,precio,tamaño,imagen,idtipo));</p>" +
                                "<strong>Listar Productos:</strong><p>LISTARPRODUCTOS();</p>" +
                            "</div>";
        
        String ayudaPedidos="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>GESTIONAR PEDIDOS</h3>" +
                                "<strong>Crear Pedido Online:</strong><p>CREARPEDIDO(idcliente,latitud,longitud);</p>" +
                              
                                "<strong>ejemplo pedido</strong><p>CREARPEDIDO(4,7854,1254);</p>" +
                                "<strong>Finalizar Pedido:</strong><p>FINALIZARPEDIDO(idpedido);</p>" +
                                "<strong>ejemplo de Finalizar Pedido:</strong><p>FINALIZARPEDIDO(8);</p>" +

                                "<strong>Entregar Pedido:</strong><p>ENTREGARPEDIDO(idpedido);</p>" +
                                "<strong>ejemplo Entregar Pedido:</strong><p>ENTREGARPEDIDO(8);</p>" +
                                "<strong>Registrar Venta:</strong><p>REGISTRARVENTA(idpedido,idVendedor);</p>" +
                                "<strong>ejemplo Registrar Venta:</strong><p>REGISTRARVENTA(8,32);</p>" +

                                "<strong>Cancelar Pedido:</strong><p>CANCELARPEDIDO(idpedido);</p>" +
                                "<strong>Cancelar Pedido:</strong><p>CANCELARPEDIDO(8);</p>" +

                                "<strong>Devolver Venta:</strong><p>DEVOLVERVENTA(idpedido,idVendedor,descripcion);</p>" +                
                                "<strong>Devolver Venta:</strong><p>DEVOLVERVENTA(8,32,no fue de su agrado);</p>" +                

                "<strong>Listar Pedidos:</strong><p>LISTARPEDIDOS();</p>" +
                
                                "<strong>Ver pedido:</strong><p>VERPEDIDO(idpedido);</p><br>" +
                                
                                "<h3>CARRITO </h3>"+
                                "<strong>Agregar al Carrito:</strong><p>AGREGARALCARRITO(idpdedido,idproducto,cantidad);</p>" +
                                "<strong>ejemplo Agregar al Carrito:</strong><p>AGREGARALCARRITO(8,1,3);</p>" +

                                "<strong>Elininar producto del Carrito:</strong><p>ElIMINARDELCARRITO(idpdedido,idproducto);</p>" +
                                "<strong>ejemplo Elininar producto del Carrito:</strong><p>ElIMINARDELCARRITO(8,1);</p>" +

                                "<strong>Actualizar producto del Carrito:</strong><p>EDITARDELCARRITO(idpdedido,idproducto,cantidad);</p>" +
                                "<strong>Ver Carrito:</strong><p>VERCARRITO(idpedido);</p>" +
                            "</div>";
        
        String ayudaVentas="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>GESTIONAR VENTAS</h3>" +
                                "<strong>Eliminar venta:</strong><p>ELIMINARVENTAS(id);</p>" +
                                "<strong>Listar ventas:</strong><p>LISTARVENTAS();</p>" +
                            "</div>";
        
        String ayudaDevoluciones="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>GESTIONAR DEVOLUCIONES</h3>" +
                                "<strong>Eliminar devolucion:</strong><p>ELIMINARDEVOLUCION(id);</p>" +
                                "<strong>Listar devoluciones:</strong><p>LISTARDEVOLUCIONES();</p>" +
                            "</div>";
        String ayudaEstadisticas="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>ESTADISTICAS</h3>" +
                                "<strong>Ver estadistica:</strong><p>VERESTADISTICAS();</p>" +
                            "</div>";
        String ayudaReportes="<div class='linea'></div>" +
                            "<div class='box'>" +
                                "<h3>REPORTES</h3>" +
                                 "<strong>Ver reportes:</strong><p>VERREPORTES();</p>" +
                            "</div>";
        
        return "Content-Type:text/html;\r\n<html>"
                +estilo               
                +titulo
                +ayudaCliente
                +ayudaVendedor
                +ayudaTipoProductos
                +ayudaProductos
                +ayudaPedidos
                +ayudaVentas
                +ayudaDevoluciones
                +ayudaReportes
                +ayudaEstadisticas
                + "</html>";
        
        
    }
    
    public String getMensajeTabla(String res){
        String estilo="<link rel='stylesheet' href='https://codepen.io/ingyas/pen/wYYaqa.css'>";
        return "Content-Type:text/html;\r\n<html>"+estilo+res+"</html>";
                
    }
 
    public String getMensajeRespuesta(String res){
        
        String estilo="<link rel='stylesheet' href='https://codepen.io/ingyas/pen/wYYaqa.css'>";
        return "Content-Type:text/html;\r\n<html>"+estilo+"<div class='alert'>" +"<strong>RESPUESTA!! </strong>"+res+"</div></html>";
    }

    
}
