/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Detalles {
   Conexion m_Conexion;
   Connection m_con;
   
   int idPedido;
   int idProducto;
   int cantidad;
   float subtotal;
   float precio;

    public Detalles() {
        
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }
   
   
   
    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public boolean registrar(){
        Statement st;
       try {
           st=m_con.createStatement();
           String sql="insert into detalles(idpedido,idproducto,cantidad,subtotal,precio)values("+getIdPedido()+","+getIdProducto()+","+getCantidad()+","+getCantidad()+"*(select precio from productos where id="+getIdProducto()+"),(select precio from productos  where id="+getIdProducto()+"))";
           //String sql="insert into detalles(idpedido,idproducto,cantidad,subtotal,precio)values(1,2,2,2*(select precio from productos where id=2),(select precio from productos  where id=2));";
           int res=st.executeUpdate(sql);
           st.close();
           if (res==1) {
               return true;
           }
           return false;
       } catch (SQLException ex) {
           Logger.getLogger(Detalles.class.getName()).log(Level.SEVERE, null, ex);
           return false;
       }
    } 
    
    
    public boolean eliminar(){
        Statement st;
       try {
           st=m_con.createStatement();
           String sql="delete from detalles where idpedido="+getIdPedido()+" and idproducto="+getIdProducto();
           int res=st.executeUpdate(sql);
           st.close();
           if (res==1) {
               return true;
           }
           return false;
       } catch (SQLException ex) {
           Logger.getLogger(Detalles.class.getName()).log(Level.SEVERE, null, ex);
           return false;
       }
    }
    
    public boolean editar(){
       Statement st;
       try {
           st=m_con.createStatement();
           String sql="update detalles set cantidad="+getCantidad()+", subtotal=precio*"+getCantidad()+" where idpedido="+getIdPedido()+" and idproducto="+getIdProducto();
           int res=st.executeUpdate(sql);
           st.close();
           if (res==1) {
               return true;
           }
           return false;
       } catch (SQLException ex) {
           Logger.getLogger(Detalles.class.getName()).log(Level.SEVERE, null, ex);
           return false;
       }
    }
   
   public ArrayList<Detalles> ver(){
       ArrayList<Detalles> list_detalles=new ArrayList<>();
       Statement st;
       try {
           st=m_con.createStatement();
           String sql="select * from detalles where idpedido="+getIdPedido();
           ResultSet res=st.executeQuery(sql);
           while (res.next()) {
              Detalles detalle =new Detalles();
              detalle.setIdPedido(res.getInt(1));
              detalle.setIdProducto(res.getInt(2));
              detalle.setCantidad(res.getInt(3));
              detalle.setSubtotal(res.getFloat(4));
              detalle.setPrecio(res.getFloat(5));
              list_detalles.add(detalle);
           }
           return list_detalles;
       } catch (SQLException ex) {
           Logger.getLogger(Detalles.class.getName()).log(Level.SEVERE, null, ex);
           
       }
       return list_detalles;
   }
   
}
