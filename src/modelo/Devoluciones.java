/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Devoluciones {

    private int id;
    private int idCliente;
    private int idVendedor;
    private float total;
    private int cantidad;
    private String fecha;
    private String hora;
    
    private Conexion m_Conexion;
    private Connection m_con;

    
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    
        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public Devoluciones() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }
    
    public ArrayList<Devoluciones> listar() {
        Statement st;
        ArrayList<Devoluciones> l_Devoluciones=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String sql="select id,idCliente,idVendedor,total,cantidad,fecha,hora from pedidos WHERE estadopedido=4";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {                
                Devoluciones m_Devolucion=new Devoluciones();
                m_Devolucion.setId(res.getInt(1));
                m_Devolucion.setIdCliente(res.getInt(2));
                m_Devolucion.setIdVendedor(res.getInt(3));
                m_Devolucion.setTotal(res.getFloat(4));
                m_Devolucion.setCantidad(res.getInt(5));
                m_Devolucion.setFecha(res.getString(6));
                m_Devolucion.setHora(res.getString(7));
                l_Devoluciones.add(m_Devolucion);
            }
            st.close();
            return l_Devoluciones;
        } catch (SQLException ex) {
            Logger.getLogger(Devoluciones.class.getName()).log(Level.SEVERE, null, ex);
            return l_Devoluciones;
        }
    }

    public boolean eliminar() {
        Statement st;
        try {
            st=m_con.createStatement();
            String sql="delete from pedidos where id="+getId()+" and estadopedido=4";
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(Devoluciones.class.getName()).log(Level.SEVERE, null, ex);
            
        return false;
        }
    }
}
