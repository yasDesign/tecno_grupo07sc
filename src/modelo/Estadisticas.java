/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Estadisticas {
    private Conexion m_Conexion;
    private Connection m_con;

    public Estadisticas() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }
    
    
    public ArrayList<Pedidos> estadisticaVentas(){
        Statement st;
        ArrayList<Pedidos> l_Pedidos=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String sql="select * from estadistica_ventas_por_tipo";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {
                Pedidos i_Pedidos=new Pedidos();
                i_Pedidos.setFecha(res.getString(1));
                i_Pedidos.setTipoPedido(res.getInt(2));
                i_Pedidos.setTotal(res.getFloat(3));
                l_Pedidos.add(i_Pedidos);
            }
            st.close();
            return l_Pedidos;
        } catch (SQLException ex) {
            Logger.getLogger(Estadisticas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l_Pedidos;
    }
    
    public ArrayList<Pedidos> estadisticasPerdidasCancelacion(){
        Statement st;
        ArrayList<Pedidos> l_Pedidos=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String sql="select * from estadistica_perdidas_por_cancelacion";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {
                Pedidos i_Pedidos=new Pedidos();
                i_Pedidos.setFecha(res.getString(1));
                i_Pedidos.setTipoPedido(res.getInt(2));
                i_Pedidos.setTotal(res.getFloat(3));
                l_Pedidos.add(i_Pedidos);
            }
            st.close();
            return l_Pedidos;
        } catch (SQLException ex) {
            Logger.getLogger(Estadisticas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l_Pedidos;
    }
    
    public ArrayList<Pedidos> estadisticasPerdidasDevolucion(){
        Statement st;
        ArrayList<Pedidos> l_Pedidos=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String sql="select * from estadistica_perdidas_por_devolucion";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {
                Pedidos i_Pedidos=new Pedidos();
                i_Pedidos.setFecha(res.getString(1));
                i_Pedidos.setTipoPedido(res.getInt(2));
                i_Pedidos.setTotal(res.getFloat(3));
                l_Pedidos.add(i_Pedidos);
            }
            st.close();
            return l_Pedidos;
        } catch (SQLException ex) {
            Logger.getLogger(Estadisticas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l_Pedidos;
    }
    
    public ArrayList<Pedidos> estadisticasPerdidasClientes(){
        Statement st;
        ArrayList<Pedidos> l_Pedidos=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String sql="select * from estadistica_pedidos_por_clientes";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {
                Pedidos i_Pedidos=new Pedidos();
                i_Pedidos.setIdCliente(res.getInt(1));
                i_Pedidos.setCantidad(res.getInt(2));
                l_Pedidos.add(i_Pedidos);
            }
            st.close();
            return l_Pedidos;
        } catch (SQLException ex) {
            Logger.getLogger(Estadisticas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l_Pedidos;
    }
   
 
    
}
