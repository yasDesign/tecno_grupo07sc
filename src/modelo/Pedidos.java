/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Pedidos {
    Conexion m_Conexion;
    Connection m_con;

    int id;
    int idCliente;
    int cantidad;
    float total;
    int tipoPedido;
    int estadoPedido;
    int idVendedor;
    String fecha;
    String hora;
    String descripcion;

    public Pedidos() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }
     
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    float latitud,longitud;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTipoPedido() {
        return tipoPedido;
    }

    public void setTipoPedido(int tipoPedido) {
        this.tipoPedido = tipoPedido;
    }

    public int getEstadoPedido() {
        return estadoPedido;
    }

    public void setEstadoPedido(int estadoPedido) {
        this.estadoPedido = estadoPedido;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }
    
    public int crear(){
        Statement st;
        try {
            st=m_con.createStatement();
            String sql="insert into pedidos(latitud,longitud,tipopedido,estadopedido,idcliente)values("+getLatitud()+","+getLongitud()+",1,0,"+getIdCliente()+");";
            st.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            if ( res.next() ) {
                int pedido_id=res.getInt(1);
                System.out.println("dato ingresaro :"+pedido_id);
                st.close();
                return pedido_id;
            }else{
                System.out.println("dato ingresaro :nada");
                st.close();
                return 0;
            }

            
        } catch (SQLException ex) {
            Logger.getLogger(Pedidos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    public int crearLocal(){
        Statement st;
        try {
            st=m_con.createStatement();
            String sql="insert into pedidos(latitud,longitud,tipopedido,estadopedido,idvendedor)values(0,0,2,0,"+getIdVendedor()+");";
            st.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            if ( res.next() ) {
                int pedido_id=res.getInt(1);
                System.out.println("dato ingresaro :"+pedido_id);
                st.close();
                return pedido_id;
            }else{
                System.out.println("dato ingresaro :nada");
                st.close();
                return 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Pedidos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public boolean finalizar(){
        try {
            Statement st=m_con.createStatement();
            String sql="update pedidos set estadopedido=1 where id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                System.out.println("finaliozacion exitosa pedido");
                return true;
            }else{
                System.out.println("error al finalizar pedido");
                return false;
            }
            
            } catch (SQLException ex) {
                Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error al realizar la consulta finalizar");
                return false;
            }
    }
    
    public boolean entregar(){
        try {
            Statement st=m_con.createStatement();
            String sql="update pedidos set estadopedido=2 where id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                System.out.println("entregando el  pedido");
                return true;
            }else{
                System.out.println("error al intentar entregar el pedido");
                return false;
            }
            
            } catch (SQLException ex) {
                Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error al realizar la consulta entregar el pedido");
                return false;
            }
    }
    
    public boolean registrar(){
           try {
            Statement st=m_con.createStatement();
            String sql="update pedidos set estadopedido=3, idvendedor="+getIdVendedor()+" where id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                System.out.println("venta exitosa pedido");
                return true;
            }else{
                System.out.println("error al vender pedido");
                return false;
            }
            
            } catch (SQLException ex) {
                Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error al realizar la consulta editar");
                return false;
            }
    }
     
    public ArrayList<Pedidos> listar(){
        
         ArrayList<Pedidos> m_clList=new ArrayList<>();
        Statement st;
         try {
            st=m_con.createStatement();
            String sql="select * from pedidos";
            ResultSet res=st.executeQuery(sql);
            while(res.next()){
                //System.out.println("id:"+res.getInt(1)+"nombre:"+res.getString(2));
                Pedidos m_pedido=new Pedidos();
                m_pedido.setId(res.getInt(1));
                m_pedido.setCantidad(res.getInt(2));
                m_pedido.setTotal(res.getFloat(3));
                m_pedido.setLatitud(res.getFloat(4));
                m_pedido.setLongitud(res.getFloat(5));
                m_pedido.setTipoPedido(res.getInt(6));
                m_pedido.setEstadoPedido(res.getInt(7));
                m_pedido.setFecha(res.getString(8));
                m_pedido.setHora(res.getString(9));
                m_pedido.setIdCliente(res.getInt(10));
                m_pedido.setIdVendedor(res.getInt(11));
               
                m_clList.add(m_pedido);
            }
            st.close();
            return m_clList;
        } catch (SQLException ex) {
            //Logger.getLogger(Productos2.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error al realizar la consulta listar");
            return m_clList;
        }  
        
    }

    public boolean devolver() {
        try {
            Statement st=m_con.createStatement();
            String sql="update pedidos set estadopedido=4, idvendedor="+getIdVendedor()+", descripcion='"+getDescripcion()+"' where id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                System.out.println("venta devuelta");
                return true;
            }else{
                System.out.println("error al devolver venta");
                return false;
            }
            
            } catch (SQLException ex) {
                Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error al realizar la consulta devolver");
                return false;
            }
    }

    public boolean cancelar() {
         try {
            Statement st=m_con.createStatement();
            String sql="update pedidos set estadopedido=5' where id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                System.out.println("pedido cancelado");
                return true;
            }else{
                System.out.println("error al cancelar pedido");
                return false;
            }
            
            } catch (SQLException ex) {
                Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("error al realizar la consulta cancelar");
                return false;
            }
    }

    public Pedidos ver() {
        Statement st;
        try {
            st=m_con.createStatement();
            String sql="select * from pedidos where id="+getId();
            Pedidos m_pedido=new Pedidos();
            ResultSet res=st.executeQuery(sql);
            while(res.next()){
                //System.out.println("id:"+res.getInt(1)+"nombre:"+res.getString(2));
                
                m_pedido.setId(res.getInt(1));
                m_pedido.setCantidad(res.getInt(2));
                m_pedido.setTotal(res.getFloat(3));
                m_pedido.setLatitud(res.getFloat(4));
                m_pedido.setLongitud(res.getFloat(5));
                m_pedido.setTipoPedido(res.getInt(6));
                m_pedido.setEstadoPedido(res.getInt(7));
                m_pedido.setFecha(res.getString(8));
                m_pedido.setHora(res.getString(9));
                m_pedido.setIdCliente(res.getInt(10));
                m_pedido.setIdVendedor(res.getInt(11));
                m_pedido.setDescripcion(res.getString(12));
                  
            }
            return m_pedido;
        } catch (SQLException ex) {
            //Logger.getLogger(Productos2.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error al realizar la consulta listar");
            //return false;
            return null;
        }  
      
    }
    
  
}
