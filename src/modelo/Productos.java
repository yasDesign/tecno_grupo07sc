/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Productos {
     private Conexion m_Conexion;
    private Connection m_Con;
    
    private int id;
    private String nombre;
    private String imagen;
    private String descripcion;

    private int tipo;
    private float tamaño;
    private float precio;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getTamaño() {
        return tamaño;
    }

    public void setTamaño(float tamaño) {
        this.tamaño = tamaño;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }


    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }



    public Productos() {
        m_Conexion=Conexion.getInstancia();
        m_Con=m_Conexion.getConexion();    
    }
        
    
    public boolean registrar(){
        Statement st;
        try {
            st = m_Con.createStatement();
            String sql_productos="insert into productos(nombre,descripcion,precio,tamaño,imagen,idtipo)values('"+getNombre()+"','"+getDescripcion()+"',"+getPrecio()+","+getTamaño()+",'"+getImagen()+"',"+getTipo()+");";
            st.executeUpdate(sql_productos,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            if ( res.next() ) {
                int cliente_id=res.getInt(1);
                System.out.println("dato ingresaro :"+cliente_id);
            }else{
                System.out.println("dato ingresaro :nada");
            }
            st.close();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        
    }
    
    public boolean eliminar(){
        Statement st;
        
        try {
            st = m_Con.createStatement();
            String sql="delete from productos where id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if ( res==1 ) {
                System.out.println("dato eliminado : true");
                return true;
            }else{
                System.out.println("dato eliminado :false");
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public boolean editar(){
        try {
            Statement st=m_Con.createStatement();
            String sql="update productos set nombre='"+getNombre()+"', descripcion='"+getDescripcion()+"', imagen='"+getImagen()+"',tipo="+getTipo()+",precio="+getPrecio()+",tamaño="+getTamaño()+" where id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                System.out.println("edicion exitosa productos");
                return true;
            }else{
                System.out.println("error al editar productos");
                return false;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error al realizar la consulta editar");
            return false;
        }        
        
    }
    
    public ArrayList<Productos> listar(){
        ArrayList<Productos> m_clList=new ArrayList<>();
        try {
            Statement st=m_Con.createStatement();
            String sql="select * from productos order by productos.id";
            ResultSet res=st.executeQuery(sql);
            while(res.next()){
                //System.out.println("id:"+res.getInt(1)+"nombre:"+res.getString(2));
                Productos m_Productos=new Productos();
                m_Productos.setId(res.getInt(1));
                m_Productos.setNombre(res.getString(2));
                m_Productos.setDescripcion(res.getString(3));
                m_Productos.setPrecio(res.getFloat(4));
                m_Productos.setTamaño(res.getFloat(5));
                m_Productos.setImagen(res.getString(6));
                m_Productos.setTipo(res.getInt(7));
                
                m_clList.add(m_Productos);
            }
            st.close();
        } catch (SQLException ex) {
            //Logger.getLogger(Productos2.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error al realizar la consulta listar");
            //return false;
        }  
        return m_clList;
    }

    @Override
    public String toString() {
        return "Productos{" + "id=" + id + ", nombre=" + nombre + ", imagen=" + imagen + ", descripcion=" + descripcion + ", tipo=" + tipo + ", tama\u00f1o=" + tamaño + ", precio=" + precio + '}';
    }

    

}
