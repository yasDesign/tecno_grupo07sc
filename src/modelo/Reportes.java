/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Reportes {
    private Conexion m_Conexion;
    private Connection m_con;

    public Reportes() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }
    
    public ArrayList<Vendedores> reporteVendedores(){
        Statement st;
        ArrayList<Vendedores> l_vendedores=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String sql="select * from reporte_vendedores";
            ResultSet res=st.executeQuery(sql);
            
            while(res.next()){
                //System.out.println("id:"+res.getInt(1)+"nombre:"+res.getString(2));
                Vendedores m_Vendedor=new Vendedores();
                m_Vendedor.setId(res.getInt(1));
                m_Vendedor.setNombre(res.getString(2));
                m_Vendedor.setImagen(res.getString(3));
                
                m_Vendedor.setCorreo(res.getString(4));
                m_Vendedor.setContrasena(res.getString(5));
                m_Vendedor.setCelular(res.getInt(6));
                m_Vendedor.setDireccion(res.getString(7));
                m_Vendedor.setLatitud(res.getFloat(8));
                m_Vendedor.setLongitud(res.getFloat(9));
                m_Vendedor.setTipo(res.getInt(10));
                m_Vendedor.setEstado(res.getInt(11));
                
                l_vendedores.add(m_Vendedor);
            
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l_vendedores;
                
    }
    
    public ArrayList<Clientes> reporteClientes(){
        Statement st;
        ArrayList<Clientes> l_clientes=new ArrayList<>();
        try {
            st=m_con.createStatement();
            
            String sql="select * from reporte_clientes";
            ResultSet res=st.executeQuery(sql);
            while(res.next()){
                Clientes m_Clientes=new Clientes();
                m_Clientes.setId(res.getInt(1));
                m_Clientes.setNombre(res.getString(2));
                m_Clientes.setImagen(res.getString(3));
                
                m_Clientes.setCorreo(res.getString(4));
                m_Clientes.setContrasena(res.getString(5));
                m_Clientes.setCelular(res.getInt(6));
                m_Clientes.setDireccion(res.getString(7));
                m_Clientes.setLatitud(res.getFloat(8));
                m_Clientes.setLongitud(res.getFloat(9));
                m_Clientes.setTipo(res.getInt(10));
                m_Clientes.setEstado(res.getInt(11));
                
                l_clientes.add(m_Clientes);
            }
            st.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l_clientes;
                
    }
 
    public ArrayList<Ventas> reporteVentas(){
        ArrayList<Ventas> list_ventas=new ArrayList<>();
        try{
            Statement st;
            st=m_con.createStatement();
            String sql="select id,idcliente,idvendedor,total,fecha,hora from reporte_ventas";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {
                Ventas m_Venta=new Ventas();
                m_Venta.setId(res.getInt(1));
                m_Venta.setIdCliente(res.getInt(2));
                m_Venta.setIdVendedor(res.getInt(3));
                m_Venta.setTotal(res.getFloat(4));
                m_Venta.setFecha(res.getString(5));
                m_Venta.setHora(res.getString(6));
                list_ventas.add(m_Venta);
            }
            return list_ventas;
        } catch (SQLException ex) {
            Logger.getLogger(Ventas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list_ventas;
    } 
    
    public ArrayList<Productos> reporteProductos(){
        ArrayList<Productos> m_clList=new ArrayList<>();
        try {
            Statement st=m_con.createStatement();
            String sql="select * from reporte_productos";
            ResultSet res=st.executeQuery(sql);
            while(res.next()){
                //System.out.println("id:"+res.getInt(1)+"nombre:"+res.getString(2));
                Productos m_Productos=new Productos();
                m_Productos.setId(res.getInt(1));
                m_Productos.setNombre(res.getString(2));
                m_Productos.setDescripcion(res.getString(3));
                m_Productos.setPrecio(res.getFloat(4));
                m_Productos.setTamaño(res.getFloat(5));
                m_Productos.setImagen(res.getString(6));
                m_Productos.setTipo(res.getInt(7));
                
                m_clList.add(m_Productos);
            }
            st.close();
        } catch (SQLException ex) {
            //Logger.getLogger(Productos2.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error al realizar la consulta listar");
            //return false;
        }  
        return m_clList;
    }

    public ArrayList<Ventas> reporteVentasOnline(){
        ArrayList<Ventas> list_ventas=new ArrayList<>();
        try{
            Statement st;
            st=m_con.createStatement();
            String sql="select id,idcliente,idvendedor,total,fecha,hora from reporte_ventas_online";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {
                Ventas m_Venta=new Ventas();
                m_Venta.setId(res.getInt(1));
                m_Venta.setIdCliente(res.getInt(2));
                m_Venta.setIdVendedor(res.getInt(3));
                m_Venta.setTotal(res.getFloat(4));
                m_Venta.setFecha(res.getString(5));
                m_Venta.setHora(res.getString(6));
                list_ventas.add(m_Venta);
            }
            return list_ventas;
        } catch (SQLException ex) {
            Logger.getLogger(Ventas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list_ventas;
    } 
     


}
