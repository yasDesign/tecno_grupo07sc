/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class TipoProductos {
    private Conexion m_Conexion;
    private Connection m_Connection;
    private int id;
    private String nombre;
    private String descripcion;
    private String imagen;

    public TipoProductos() {
        m_Conexion=Conexion.getInstancia();
        m_Connection=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
    
    public boolean registrar(){
        Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="INSERT INTO public.tipo(nombre, descripcion, imagen) VALUES ('"+getNombre()+"', '"+getDescripcion()+"','"+getImagen()+"');";
            
            boolean b_res=st.execute(s_sql);
            st.close();
            if (b_res) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(TipoProductos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public boolean eliminar(){
        Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="DELETE FROM public.tipo WHERE id="+getId();
            boolean b_res=st.execute(s_sql);
            st.close();
            if (b_res) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(TipoProductos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public boolean editar(){
        Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="UPDATE public.tipo SET nombre='"+getNombre()+"', descripcion='"+getDescripcion()+"', imagen='"+getImagen()+"' WHERE id="+getId();
            boolean b_res=st.execute(s_sql);
            st.close();
            if (b_res) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(TipoProductos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public ArrayList<TipoProductos> listar(){
       ArrayList<TipoProductos> l_tipoProductos=new  ArrayList<>();
       Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="SELECT id, nombre, descripcion, imagen FROM public.tipo;";
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                TipoProductos i_TipoProductos=new TipoProductos();
                i_TipoProductos.setId(r_res.getInt(1));
                i_TipoProductos.setNombre(r_res.getString(2));
                i_TipoProductos.setDescripcion(r_res.getString(3));
                i_TipoProductos.setImagen(r_res.getString(4));
                l_tipoProductos.add(i_TipoProductos);
            }
            st.close();
            return l_tipoProductos;
        } catch (SQLException ex) {
            Logger.getLogger(TipoProductos.class.getName()).log(Level.SEVERE, null, ex);
            return l_tipoProductos;
        }
       
    }
    
}
