/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Vendedores {
     private Conexion m_Conexion;
    private Connection m_Con;
    
    private int id;
    private String nombre;
    private String imagen;
    private String correo;
    private String contrasena;
    private int celular;
    private int estado;
    private int tipo;
    
    private float latitud;
    private float longitud;
    private String direccion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    

    public Vendedores() {
        m_Conexion=Conexion.getInstancia();
        m_Con=m_Conexion.getConexion();    
    }
    
    
    
    public boolean registrar(){
        Statement st;
        try {
            st = m_Con.createStatement();
            String sql_usuario="INSERT INTO usuarios(nombre,imagen,correo,contrasena,celular,direccion,latitud,longitud,tipo) values('"+getNombre()+"','"+getImagen()+"','"+getCorreo()+"','"+getContrasena()+"',"+getCelular()+",'"+getDireccion()+"',"+getLatitud()+","+getLongitud()+",1);";
            st.executeUpdate(sql_usuario,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            if ( res.next() ) {
                int cliente_id=res.getInt(1);
                System.out.println("dato ingresaro :"+cliente_id);
            }else{
                System.out.println("dato ingresaro :nada");
            }
            st.close();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Vendedores.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        
    }
    
    public boolean eliminar(){
        Statement st;
        
        try {
            st = m_Con.createStatement();
            String sql="delete from usuarios where tipo=1 and id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if ( res==1 ) {
                System.out.println("dato eliminado : true");
                return true;
            }else{
                System.out.println("dato eliminado :false");
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Vendedores.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public boolean editar(){
        try {
            Statement st=m_Con.createStatement();
            String sql="update usuarios set nombre='"+getNombre()+"', correo='"+getCorreo()+"',contrasena='"+getContrasena()+"', imagen='"+getImagen()+"',celular="+getCelular()+",latitud="+getLatitud()+",longitud="+getLongitud()+",direccion='"+getDireccion()+"' where tipo=1 and id="+getId();
            int res=st.executeUpdate(sql);
            st.close();
            if (res==1) {
                System.out.println("edicion exitosa usuario");
                return true;
            }else{
                System.out.println("error al editar usuario");
                return false;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Vendedores.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error al realizar la consulta editar");
            return false;
        }        
        
    }
    
    public ArrayList<Vendedores> listar(){
        ArrayList<Vendedores> m_clList=new ArrayList<>();
        try {
            Statement st=m_Con.createStatement();
            String sql="select * from usuarios where tipo=1 order by usuarios.id";
            ResultSet res=st.executeQuery(sql);
            
            while(res.next()){
                //System.out.println("id:"+res.getInt(1)+"nombre:"+res.getString(2));
                Vendedores m_Vendedor=new Vendedores();
                m_Vendedor.setId(res.getInt(1));
                m_Vendedor.setNombre(res.getString(2));
                m_Vendedor.setImagen(res.getString(3));
                
                m_Vendedor.setCorreo(res.getString(4));
                m_Vendedor.setContrasena(res.getString(5));
                m_Vendedor.setCelular(res.getInt(6));
                m_Vendedor.setDireccion(res.getString(7));
                m_Vendedor.setLatitud(res.getFloat(8));
                m_Vendedor.setLongitud(res.getFloat(9));
                m_Vendedor.setTipo(res.getInt(10));
                m_Vendedor.setEstado(res.getInt(11));
                
                m_clList.add(m_Vendedor);
            }
            st.close();
        } catch (SQLException ex) {
            //Logger.getLogger(Vendedor2.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error al realizar la consulta listar");
            //return false;
        }  
        return m_clList;
    }

    @Override
    public String toString() {
        return "Vendedor2{" + "id=" + id + ", nombre=" + nombre + ", imagen=" + imagen + ", correo=" + correo + ", contrasena=" + contrasena + ", celular=" + celular + ", estado=" + estado + ", tipo=" + tipo + ", latitud=" + latitud + ", longitud=" + longitud + ", direccion=" + direccion + '}';
    }

}
