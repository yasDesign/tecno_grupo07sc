/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Ventas {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int id;
    private int idCliente;
    private int idVendedor;
    private float total;
    private String fecha;
    private String hora;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    
    public Ventas() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }
    
    public boolean eliminar(){
        try{
            Statement st;
            st=m_con.createStatement();
            String sql="delect from pedidos where estadopedido=3 and id="+getId();
            //ResultSet res=st.executeQuery(sql);
            boolean res=st.execute(sql);
            if (res) {
                return true;
            }
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Ventas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public ArrayList<Ventas> listar(){
        ArrayList<Ventas> list_ventas=new ArrayList<>();
        try{
            Statement st;
            st=m_con.createStatement();
            String sql="select id,idcliente,idvendedor,total,fecha,hora from pedidos where estadopedido=3";
            ResultSet res=st.executeQuery(sql);
            while (res.next()) {
                Ventas m_Venta=new Ventas();
                m_Venta.setId(res.getInt(1));
                m_Venta.setIdCliente(res.getInt(2));
                m_Venta.setIdVendedor(res.getInt(3));
                m_Venta.setTotal(res.getFloat(4));
                m_Venta.setFecha(res.getString(5));
                m_Venta.setHora(res.getString(6));
                list_ventas.add(m_Venta);
            }
            return list_ventas;
        } catch (SQLException ex) {
            Logger.getLogger(Ventas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list_ventas;
    } 
    
}
