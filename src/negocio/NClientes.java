/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Clientes;

/**
 *
 * @author tecno
 */
public class NClientes {

    private Clientes m_Clientes;

    public NClientes() {
        m_Clientes = new Clientes();
    }

    public String registrar(String[] p_parametros) {
        if (validarParaRegistrar(p_parametros)) {
            boolean res = false;
            m_Clientes.setNombre(p_parametros[0]);
            m_Clientes.setImagen(p_parametros[1]);
            m_Clientes.setCorreo(p_parametros[2]);
            m_Clientes.setContrasena(p_parametros[3]);
            m_Clientes.setCelular(Integer.parseInt(p_parametros[4]));
            m_Clientes.setDireccion(p_parametros[5]);
            m_Clientes.setLatitud(Float.parseFloat(p_parametros[6]));
            m_Clientes.setLongitud(Float.parseFloat(p_parametros[7]));

            res = m_Clientes.registrar();
            if (res) {
                System.out.println("cliente registrado exitosamente");
                return "cliente registrado exitosamente";
            } else {
                System.out.println("error al registrar cliente");
                return "error al registrar cliente";
            }
        } else {
            System.out.println("ERROR: parametros no validos revisar la ayuda!!");
            return "ERROR: parametros no validos revisar la ayuda!!";
        }

    }

    public String eliminar(String[] p_parametros) {
        if (validarParaEliminar(p_parametros)) {
            boolean res = false;
            m_Clientes.setId(Integer.parseInt(p_parametros[0]));
            res = m_Clientes.eliminar();
            if (res) {
                System.out.println("cliente eliminado exitosamente");
                return "cliente eliminado exitosamente";
            } else {
                System.out.println("error al eliminar cliente");
                return "error al eliminar cliente";
            }
        } else {
            System.out.println("ERROR: parametros no validos revisar la ayuda!!");
            return "ERROR: parametros no validos revisar la ayuda!!";
        }

    }

    public String editar(String[] p_parametros) {
        if (validarParaEditar(p_parametros)) {
            boolean res = false;
            m_Clientes.setId(Integer.parseInt(p_parametros[0]));
            m_Clientes.setNombre(p_parametros[1]);
            m_Clientes.setImagen(p_parametros[2]);
            m_Clientes.setCorreo(p_parametros[3]);
            m_Clientes.setContrasena(p_parametros[4]);
            m_Clientes.setCelular(Integer.parseInt(p_parametros[5]));
            m_Clientes.setDireccion(p_parametros[6]);
            m_Clientes.setLatitud(Float.parseFloat(p_parametros[7]));
            m_Clientes.setLongitud(Float.parseFloat(p_parametros[8]));
            res = m_Clientes.editar();
            if (res) {
                System.out.println("cliente editado exitosamente");
                return "cliente editar exitosamente";
            } else {
                System.out.println("error al editar cliente");
                return "error al editar cliente";
            }
        } else {
            System.out.println("ERROR: parametros no validos revisar la ayuda!!");
            return "ERROR: parametros no validos revisar la ayuda!!";
        }
    }

    public String listar() {

        ArrayList<Clientes> res = m_Clientes.listar();
        String res_clientes="";
        if (res.size() > 0) {
            res_clientes="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                            "<th>nombre</th>" +
                            "<th>correo</th>" +
                            "<th>contrasena</th>" +
                            "<th>imagen</th>" +
                            "<th>celular</th>" +
                            "<th>latitud</th>" +
                            "<th>longitud</th>" +
                            "<th>estado</th>" +
                        "</tr>"
                        + "</thead>"
                        + "<tbody>";
            for (Clientes re : res) {
                String res_local="<tr>"
                        +"<td>"+re.getId()+"</td>"
                        +"<td>"+re.getNombre()+"</td>"
                        +"<td>"+re.getCorreo()+"</td>"
                        +"<td>"+re.getContrasena()+"</td>"
                        +"<td>"+re.getImagen()+"</td>"
                        +"<td>"+re.getCelular()+"</td>"
                        +"<td>"+re.getLatitud()+"</td>"
                        +"<td>"+re.getLongitud()+"</td>"
                        +"<td>"+re.getEstado()+"</td>"
                        + "</tr>";
                res_clientes=res_clientes+res_local;
            }
            res_clientes=res_clientes+"</tbody></table>";
            return "<H2>lista de clientes</H2>"+res_clientes;
        } else {
            System.out.println("lista vacia");
            return "<div class='alert'> <strong>RESPUESTA!! </strong>Lista de clientes vacia</div>";
        }

    }

    private boolean validarParaRegistrar(String[] p_parametros) {
        ///validar cantidad de parametros
        int cantidad_parametrs = p_parametros.length;
        if (cantidad_parametrs != 8) {
            System.out.println("cantidad no validad");
            return false;
        }
        
        //validar parametros son correo
        if (!esCorreo(p_parametros[2])) {
            System.out.println("parametro no es correo");
            return false;
        }
        //validar parametros son numeros
        if (!esNumero(p_parametros[4])) {
            System.out.println("parametro no es numero");
            return false;
        }
        //validar parametros son flotantes
        if (!esFlotante(p_parametros[6]) || !esFlotante(p_parametros[7])) {
            System.out.println("parametros no son ubicacion");
            return false;
        }
        return true;
    }

    private boolean esCadena(String p_parametro) {
        return true;
    }

    private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean esFlotante(String s) {
        boolean isValid = true;
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException nfe) {
            isValid = false;
        }
        return isValid;
    }

    private boolean esCorreo(String p_parametro) {
        String emailPatron = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        Pattern pattern = Pattern.compile(emailPatron);
        Matcher matcher = pattern.matcher(p_parametro);
        if (matcher.matches()) {
            System.out.println("correo valido");
            return true;
        } else {
            System.out.println("correo no validao");
            return false;
        }

    }

    private boolean validarParaEditar(String[] p_parametros) {
        ///validar cantidad de parametros
        int cantidad_parametrs = p_parametros.length;
        if (cantidad_parametrs != 9) {
            System.out.println("cantidad no validad");
            return false;
        }
        //validar parametros son correo
        if (!esCorreo(p_parametros[3])) {
            System.out.println("parametro no es correo");
            return false;
        }
        //validar parametros son numeros
        if (!esNumero(p_parametros[5]) || !esNumero(p_parametros[0])) {
            System.out.println("parametro no es numero");
            return false;
        }
        return true;
    }

     private boolean validarParaEliminar(String[] p_parametros) {
        ///validar cantidad de parametros
        int cantidad_parametrs = p_parametros.length;
        if (cantidad_parametrs < 1) {
            System.out.println("cantidad no validad");
            return false;
        }
      
        //validar parametros son numeros
        if (!esNumero(p_parametros[0])) {
            System.out.println("parametro no es numero");
            return false;
        }
        return true;
    }

}
