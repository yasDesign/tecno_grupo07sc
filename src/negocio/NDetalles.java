/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Detalles;

/**
 *
 * @author tecno
 */
public class NDetalles {

    private Detalles m_Detalle;
    public NDetalles() {
        m_Detalle=new Detalles();
    }
    
    public String registrar(String[] p_parametros){
        if (validarParaRegistrar(p_parametros)) {
            m_Detalle.setIdPedido(Integer.parseInt(p_parametros[0]));
            m_Detalle.setIdProducto(Integer.parseInt(p_parametros[1]));
            m_Detalle.setCantidad(Integer.parseInt(p_parametros[2]));
            boolean res=m_Detalle.registrar();
            if (!res) {
                return "Producot agregado al carrito";
            }else{
                return "ocurrio un error al insertar el producto";
            }
        }
        return "no se pudo agregar al carrito ver los parametros q sean validos";
    }
    
    public String editar(String[] p_parametros){
            if (validarParaRegistrar(p_parametros)) {
            m_Detalle.setIdPedido(Integer.parseInt(p_parametros[0]));
            m_Detalle.setIdProducto(Integer.parseInt(p_parametros[1]));
            m_Detalle.setCantidad(Integer.parseInt(p_parametros[2]));
            boolean res=m_Detalle.registrar();
            if (!res) {
                return "Producto actualizado del carrito";
            }else{
                return "ocurrio un error al editar el producto";
            }
        }
        return "no se pudo editar del carrito ver los parametros q sean validos";
    
    }
    
    public String eliminar(String[] p_parametros){
            if (validarParaEliminar(p_parametros)) {
            m_Detalle.setIdPedido(Integer.parseInt(p_parametros[0]));
            m_Detalle.setIdProducto(Integer.parseInt(p_parametros[1]));
            boolean res=m_Detalle.eliminar();
            if (!res) {
                return "Producto eliminado del carrito";
            }else{
                return "ocurrio un error al eliminar el producto";
            }
        }
        return "no se pudo eliminar del carrito ver los parametros q sean validos";
    }
    
    public String ver(String[] p_parametros){
        if (validarParaVer(p_parametros)) {
            m_Detalle.setIdPedido(Integer.parseInt(p_parametros[0]));
            ArrayList<Detalles> lista_detalles=m_Detalle.ver();
            if (lista_detalles.size()>0) {
              String  res_producto="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                                +"<thead style='background-color:#008080; color:white;'>" +
                                "<tr><th>idPedido</th>" +
                                "<th>idproducto</th>" +
                                "<th>cantidad</th>" +
                                "<th>subtotal</th>" +
                                "<th>precio</th>" +
                                "</tr></thead>"
                            + "<tbody>";
                    for (Detalles i_detalle : lista_detalles) {
                        String res_local="<tr>"
                                +"<td>"+i_detalle.getIdPedido()+"</td>"
                                +"<td>"+i_detalle.getIdProducto()+"</td>"
                                +"<td>"+i_detalle.getCantidad()+"</td>"
                                +"<td>"+i_detalle.getSubtotal()+"</td>"
                                +"<td>"+i_detalle.getPrecio()+"</td>"                                
                                + "</tr>";
                        res_producto=res_producto+res_local;
                    }
                    res_producto=res_producto+"</tbody></table>";
                    return res_producto;

            }
            return "<h4>Carrito vacio no tiene elementos agregados</h4>";            
        }
        return "no se puede ver ese carrito verificar los parametros sean validos";
    }
    
    
    
    /************************************************************************/
    /*******************VALIDACIONES****************************************/
    /************************************************************************/
    /************************************************************************/
    
    private boolean validarParaRegistrar(String[] p_parametros) {
        if (p_parametros.length==3 && esNumero(p_parametros[0]) && esNumero(p_parametros[1]) && esNumero(p_parametros[2])) {
            return true;
        }
        return true;
    }

    private boolean validarParaEliminar(String[] p_parametros) {
        if (p_parametros.length==2 && esNumero(p_parametros[0]) && esNumero(p_parametros[1])) {
            return true;
        }
        return true;
    }

    private boolean validarParaVer(String[] p_parametros) {
        if (p_parametros.length==2 && esNumero(p_parametros[0]) && esNumero(p_parametros[1])) {
            return true;
        }
        return true;
    }

    private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
