/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Devoluciones;

/**
 *
 * @author tecno
 */
public class NDevoluciones {

    private Devoluciones m_Devolucion;
    public NDevoluciones() {
        m_Devolucion=new Devoluciones();
    }
    
    public String eliminar(String[] p_parametros){
        if (validarParaEliminar(p_parametros)) {
            m_Devolucion.setId(Integer.parseInt(p_parametros[0]));
            boolean res=m_Devolucion.eliminar();
            if(res){
                return "la devolucion se ha eliminado satisfactoriamente";
            }
            return "Error al intentar eliminar la devolucion";
        }
        return "No se pudo eliminar la devolucion verifica los parametros";
    }
    
    public String listar(){
        ArrayList<Devoluciones> l_devolucion=m_Devolucion.listar();
        if (l_devolucion.size()>0) {
            String s_devolucion="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>idcliente</th>" +
                        "<th>idvendedor</th>" +
                        "<th>total</th>" +
                        "<th>cantidad</th>" +
                        "<th>fecha</th>" +
                        "<th>hora</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Devoluciones re : l_devolucion) {
                String res_local="<tr>"
                        +"<td>"+re.getId()+"</td>"
                        +"<td>"+re.getIdCliente()+"</td>"
                        +"<td>"+re.getIdVendedor()+"</td>"
                        +"<td>"+re.getTotal()+"</td>"
                        +"<td>"+re.getCantidad()+"</td>"
                        +"<td>"+re.getFecha()+"</td>"
                        +"<td>"+re.getHora()+"</td>"
                        + "</tr>";
                s_devolucion=s_devolucion+res_local;
            }
            s_devolucion=s_devolucion+"</tbody></table>";
            return "<H2>lista de devoluciones</H2>"+s_devolucion;    

        }
        return "<h4>La lista de devoluciones esta vacia</h4>";
        }
    
    /**********************************************************************/
    /*********************VALIDACIONES**************************/
    /**********************************************************************/
    
    private boolean validarParaEliminar(String[] p_parametros) {
        if (p_parametros.length==1 && esNumero(p_parametros[0])) {
            return true;
        }
        return false;
    }

    private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
}
