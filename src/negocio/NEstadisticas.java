/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Clientes;
import modelo.Estadisticas;
import modelo.Pedidos;

/**
 *
 * @author tecno
 */
public class NEstadisticas {
    private Estadisticas m_Estadistica;

    public NEstadisticas() {
        m_Estadistica=new Estadisticas();
    }
    
    public String verEstadisticas(){
        return "<H2>ESTADISTICAS</H2>"+"<h3>estadistica de ventas</h3>"+estadisticaVentas()+
                "<h3>estadistica de Perdidas por cancelaciones</h3>"+estadisticasPerdidasCancelacion()+
                "<h3>estadistica de Perdidas por Clientes</h3>"+estadisticasPerdidasClientes()+
                "<h3>estadistica de Perdidas por Devoluciones</h3>"+estadisticasPerdidasDevolucion();
    }
    
    private String estadisticaVentas(){
        
        ArrayList<Pedidos> l_Pedidos=m_Estadistica.estadisticaVentas();
        if (l_Pedidos.size()>0) {
            String s_pedidos="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>fecha</th>" +
                        "<th>tipoPedido</th>" +
                        "<th>total</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Pedidos i_Pedido : l_Pedidos) {
                String res_local="<tr>"
                        +"<td>"+i_Pedido.getFecha()+"</td>"
                        +"<td>"+i_Pedido.getTipoPedido()+"</td>"
                        +"<td>"+i_Pedido.getTotal()+"</td>"                       
                        + "</tr>";
                s_pedidos=s_pedidos+res_local;
            }
            s_pedidos=s_pedidos+"</tbody></table>";
 
            return s_pedidos;
        }
        return "<h4>Estadistica de Ventas esta Vacia</h4>";
   

    }
    
    private String estadisticasPerdidasCancelacion(){
            
        ArrayList<Pedidos> l_Pedidos=m_Estadistica.estadisticasPerdidasCancelacion();
        if (l_Pedidos.size()>0) {
            String s_pedidos="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>fecha</th>" +
                        "<th>tipoPedido</th>" +
                        "<th>total</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Pedidos i_Pedido : l_Pedidos) {
                String res_local="<tr>"
                        +"<td>"+i_Pedido.getFecha()+"</td>"
                        +"<td>"+i_Pedido.getTipoPedido()+"</td>"
                        +"<td>"+i_Pedido.getTotal()+"</td>"                       
                        + "</tr>";
                s_pedidos=s_pedidos+res_local;
            }
            s_pedidos=s_pedidos+"</tbody></table>";
 
            return s_pedidos;
        }
        return "<h4>Estadistica de Perdidas por Cancelacion del Cliente esta Vacia</h4>";
   
    }
    
    private String estadisticasPerdidasDevolucion(){
       
        ArrayList<Pedidos> l_Pedidos=m_Estadistica.estadisticasPerdidasDevolucion();
        if (l_Pedidos.size()>0) {
            String s_pedidos="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>fecha</th>" +
                        "<th>tipoPedido</th>" +
                        "<th>cantidad</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Pedidos i_Pedido : l_Pedidos) {
                String res_local="<tr>"
                        +"<td>"+i_Pedido.getFecha()+"</td>"
                        +"<td>"+i_Pedido.getTipoPedido()+"</td>"
                        +"<td>"+i_Pedido.getCantidad()+"</td>"                       
                        + "</tr>";
                s_pedidos=s_pedidos+res_local;
            }
            s_pedidos=s_pedidos+"</tbody></table>";
 
            return s_pedidos;
        }
        return "<h4>Estadistica de Perdidas por Devolucion del Cliente esta Vacia</h4>";
   
    }
    
    private String estadisticasPerdidasClientes(){
        
        ArrayList<Pedidos> l_Pedidos=m_Estadistica.estadisticasPerdidasClientes();
        if (l_Pedidos.size()>0) {
            String s_pedidos="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>cantidad</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Pedidos i_Pedido : l_Pedidos) {
                String res_local="<tr>"
                        +"<td>"+i_Pedido.getIdCliente()+"</td>"
                        +"<td>"+i_Pedido.getCantidad()+"</td>"                       
                        + "</tr>";
                s_pedidos=s_pedidos+res_local;
            }
            s_pedidos=s_pedidos+"</tbody></table>";
 
            return s_pedidos;
        }
        return "<h4>Estadistica de Perdidas por Cliente esta Vacia</h4>";
    }    
    
    
}
