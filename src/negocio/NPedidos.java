/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Pedidos;

/**
 *
 * @author tecno
 */
public class NPedidos {
    Pedidos m_pedido;

    public NPedidos() {
        m_pedido=new Pedidos();
    }
    
    public String crear(String[] p_parametros){
        if (validarParaCrearOnLine(p_parametros)) {
            m_pedido.setIdCliente(Integer.parseInt(p_parametros[0]));
            m_pedido.setLatitud(Float.parseFloat(p_parametros[1]));
            m_pedido.setLongitud(Float.parseFloat(p_parametros[2]));
            int res=m_pedido.crear();
            if (res==0) {
                return "el pedido no pudo ser creado intente otra vez";
            }
            return "pedido creado nro="+res;
        }
        return "revise los parametros";
        
    }
    
    public String crearLocal(String[] p_parametros){
        if (validarParaCrear(p_parametros)) {
            m_pedido.setIdVendedor(Integer.parseInt(p_parametros[0]));
            int res=m_pedido.crearLocal();
            if (res==0) {
                return "el pedido no pudo ser creado intente otra vez";
            }
            return "pedido creado nro="+res;
        }
        return "revise los parametros";
        
    }
    
    public String finalizar(String[] p_parametros){
        if (validarParaCrear(p_parametros)) {
            m_pedido.setId(Integer.parseInt(p_parametros[0]));
            boolean res=m_pedido.finalizar();
            if (! res) {
                return "el pedido no pudo ser enviarse intente otra vez";
            }
            return "pedido creado enviado exitosamente";
        }
        return "revise los parametros";
    }
    
    public String entregar(String[] p_parametros){
        if (validarParaCrear(p_parametros)) {
            m_pedido.setId(Integer.parseInt(p_parametros[0]));
            boolean res=m_pedido.entregar();
            if (! res) {
                return "el pedido no pudo ser entregado intente otra vez";
            }
            return "pedido entregando exitosamente";
        }
        return "revise los parametros";
    } 
   
    public String registrar(String[] p_parametros){
        if (validarParaEditar(p_parametros)) {
            m_pedido.setId(Integer.parseInt(p_parametros[0]));
            m_pedido.setIdVendedor(Integer.parseInt(p_parametros[1]));
            boolean res=m_pedido.registrar();
            if (!res) {
                return "la venta no pudo ser registrada";
            }
            return "venta registrada exitosamente";
        }
        return "revise los parametros";
    }
    
    public String devolucion(String[] p_parametros){
        if (validarParaDevolver(p_parametros)) {
            m_pedido.setId(Integer.parseInt(p_parametros[0]));
            m_pedido.setIdVendedor(Integer.parseInt(p_parametros[1]));
            m_pedido.setDescripcion(p_parametros[2]);
            boolean res=m_pedido.devolver();
            if (!res) {
                return "la venta no puedo ser devuelta";
            }
            return "la venta fue devuelta";
        }
        return "revise los parametros";
    }
    
    public String cancelar(String[] p_parametros){
        if (validarParaCrear(p_parametros)) {
            m_pedido.setId(Integer.parseInt(p_parametros[0]));
            boolean res=m_pedido.cancelar();
            if (!res) {
                return "el pedido no puedo ser cancelada";
            }
            return "la venta fue cancelada";
        }
        return "revise los parametros";
    } 
    
    public String ver(String[] p_parametros){
        if (validarParaCrear(p_parametros)) {
            m_pedido.setId(Integer.parseInt(p_parametros[0]));
            m_pedido=m_pedido.ver();
            return m_pedido.toString();
        }
        
        return "parametros no validos";
    }
    
    public String listar(){
        
        ArrayList<Pedidos> res = m_pedido.listar();
        
        String res_pedido="";
        if (res.size() > 0) {
            res_pedido="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>cantidad</th>" +
                        "<th>total</th>" +
                        "<th>latitud</th>" +
                        "<th>longitud</th>" +
                        "<th>tipopedido</th>" +
                        "<th>estadopedido</th>" +
                        "<th>fecha</th>" +
                        "<th>hora</th>" +
                        "<th>idCliente</th>" +
                        "<th>idvendedor</th>" +
                        "<th>descripcion</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Pedidos re : res) {
                String res_local="<tr>"
                        +"<td>"+re.getId()+"</td>"
                        +"<td>"+re.getCantidad()+"</td>"                       
                        +"<td>"+re.getTotal()+"</td>"
                        +"<td>"+re.getLatitud()+"</td>"
                        +"<td>"+re.getLongitud()+"</td>"
                        +"<td>"+re.getTipoPedido()+"</td>"
                        +"<td>"+re.getEstadoPedido()+"</td>"
                        +"<td>"+re.getFecha()+"</td>"
                        +"<td>"+re.getHora()+"</td>"
                        +"<td>"+re.getIdCliente()+"</td>"
                        +"<td>"+re.getIdVendedor()+"</td>"
                        +"<td>"+re.getDescripcion()+"</td>"
                        + "</tr>";
                res_pedido=res_pedido+res_local;
            }
            res_pedido=res_pedido+"</tbody></table>";
            return "<H2>LISTA DE PEDIDOS</H2>"+res_pedido;
            //return "list producto  exitosamente";
        } else {
            System.out.println("error al registrar pedido");
            return "<h4>lista de pedidos vacia</h4>";
        }

    }
    //validacionn de parametos
    private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean validarParaCrear(String[] p_parametros) {
        if (p_parametros.length==1 && esNumero(p_parametros[0])) {
            return true;
        }
        return false;
    }

    private boolean validarParaEditar(String[] p_parametros){
        if (p_parametros.length==2 && esNumero(p_parametros[0]) && esNumero(p_parametros[1])) {
            return true;
        }
        return false;
    }

    private boolean validarParaCrearOnLine(String[] p_parametros) {
        if (p_parametros.length==3 && esNumero(p_parametros[0])&&esFlotante(p_parametros[1])&&esFlotante(p_parametros[2])) {
            return true;
        }
        return false;
    }

    private boolean esFlotante(String p_parametro) {
        try {
            Float.parseFloat(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean validarParaDevolver(String[] p_parametros) {
        if (p_parametros.length==3 && esNumero(p_parametros[0]) && esNumero(p_parametros[1])) {
            return true;
        }
        return false;
    }
    /*
    public static void main(String[] args) {
        NPedidos m=new NPedidos();
        String[] asd=new String[1];
        asd[0]="1";
        //System.out.println(m.ver(asd).toString());
        System.out.println(m.listar());
    }
    */
}
