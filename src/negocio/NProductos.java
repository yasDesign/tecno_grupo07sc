/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Productos;

/**
 *
 * @author tecno
 */
public class NProductos {
    Productos m_Producto;

    public NProductos() {
        m_Producto=new Productos();
    }
    
    public String registrar(String[] p_parametros){
        if (validarParaRegistrar(p_parametros)) {
            m_Producto.setNombre(p_parametros[0]);
            m_Producto.setDescripcion(p_parametros[1]);
            m_Producto.setPrecio(Float.parseFloat(p_parametros[2]));
            m_Producto.setTamaño(Float.parseFloat(p_parametros[3]));
            m_Producto.setImagen(p_parametros[4]);
            m_Producto.setTipo(Integer.parseInt(p_parametros[5]));
            boolean res=m_Producto.registrar();
            if (res) {
                return "registro exitoso de producto";
            }
             return "fallo en el registro de producto";
        }
        return "fallo en el registro de producto por error en los parametros";
    }
    
    public String editar(String[] p_parametros){
        if (validarParaEditar(p_parametros)) {
            m_Producto.setId(Integer.parseInt(p_parametros[0]));
            m_Producto.setNombre(p_parametros[1]);
            m_Producto.setDescripcion(p_parametros[2]);
            m_Producto.setPrecio(Float.parseFloat(p_parametros[3]));
            m_Producto.setTamaño(Float.parseFloat(p_parametros[4]));
            m_Producto.setImagen(p_parametros[5]);
            m_Producto.setTipo(Integer.parseInt(p_parametros[6]));
            boolean res=m_Producto.editar();
            if (res) {
                return "edicion exitoso de producto";
            }
            return "fallo en el edicion de producto";
        }
        return "fallo en el edicion por parametros no validos del producto";
    }
    
    public String eliminar(String[] p_parametros){
        if(validarParaEliminar(p_parametros)){
            m_Producto.setId(Integer.parseInt(p_parametros[0]));
            boolean res=m_Producto.eliminar();
            if (res) {
                return "edicion exitoso de producto";
            }
            return "fallo en el edicion de producto";
        }
        return "fallo en la eliminacion por parametros no validos del producto";
        
    }
    
    
    public String listar(){
        
        ArrayList<Productos> res = m_Producto.listar();
        String res_producto="";
        if (res.size() > 0) {
            res_producto="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>nombre</th>" +
                        "<th>descripcion</th>" +
                        "<th>precio</th>" +
                        "<th>tamaño</th>" +
                        "<th>imagen</th>" +
                        "<th>idtipo</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Productos re : res) {
                String res_local="<tr>"
                        +"<td>"+re.getId()+"</td>"
                        +"<td>"+re.getNombre()+"</td>"
                        +"<td>"+re.getDescripcion()+"</td>"
                        +"<td>"+re.getPrecio()+"</td>"
                        +"<td>"+re.getTamaño()+"</td>"
                        +"<td>"+re.getImagen()+"</td>"
                        +"<td>"+re.getTipo()+"</td>"
                        + "</tr>";
                res_producto=res_producto+res_local;
            }
            res_producto=res_producto+"</tbody></table>";
            return "<H2>lista de productos</H2>"+res_producto;
            //return "list producto  exitosamente";
        } else {
            System.out.println("error al registrar producto");
            return "<h4>lista de productos vacia</h4>";
        }

    }
    
    
    /***************************************************************************/
    /*******************************VALIDACIONES********************************/
    /***************************************************************************/
    private boolean validarParaRegistrar(String[] p_parametros) {
        if (p_parametros.length==6 && esFlotante(p_parametros[2]) && esFlotante(p_parametros[3]) && esNumero(p_parametros[5])) {
            return true;
        }
        return false;
    }

     private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean esFlotante(String s) {
        boolean isValid = true;
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException nfe) {
            isValid = false;
        }
        return isValid;
    }

    private boolean validarParaEditar(String[] p_parametros) {
        if (p_parametros.length==6 && esFlotante(p_parametros[2]) && esFlotante(p_parametros[3]) && esNumero(p_parametros[0])&& esNumero(p_parametros[6])) {
            return true;
        }
        return false;
    
    }

    private boolean validarParaEliminar(String[] p_parametros) {
        if (p_parametros.length==1 && esNumero(p_parametros[0])) {
            return true;
        }
        return false;
    }
    
}
