/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Clientes;
import modelo.Productos;
import modelo.Ventas;
import modelo.Reportes;
import modelo.Vendedores;
import modelo.Ventas;

/**
 *
 * @author tecno
 */
public class NReportes {
    private Reportes m_Reportes;
    
    public NReportes() {
        m_Reportes=new Reportes();
    }
    
    public String VerReporte(){
        return "<H2>REPORTES</H2>"+"<h3>Reporte de clientes</h3>"+reporteClientes()+
                "<h3>Reporte de Vendedores</h3>"+reporteVendedores()+
                "<h3>Reporte de Ventas</h3>"+reporteVentas()+
                "<h3>Reporte de Productos</h3>"+reporteproductos()+
                "<h3>Reporte de Ventas Online</h3>"+reporteVentasOnline();
    }
    
    private String reporteClientes() {

        ArrayList<Clientes> l_clientes = m_Reportes.reporteClientes();
        String s_clientes="";
        if (l_clientes.size() > 0) {
            s_clientes="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                            "<th>nombre</th>" +
                            "<th>correo</th>" +
                            "<th>contrasena</th>" +
                            "<th>imagen</th>" +
                            "<th>celular</th>" +
                            "<th>latitud</th>" +
                            "<th>longitud</th>" +
                            "<th>estado</th>" +
                        "</tr>"
                        + "</thead>"
                        + "<tbody>";
            for (Clientes i_cliente : l_clientes) {
                String l_ventas_local="<tr>"
                        +"<td>"+i_cliente.getId()+"</td>"
                        +"<td>"+i_cliente.getNombre()+"</td>"
                        +"<td>"+i_cliente.getCorreo()+"</td>"
                        +"<td>"+i_cliente.getContrasena()+"</td>"
                        +"<td>"+i_cliente.getImagen()+"</td>"
                        +"<td>"+i_cliente.getCelular()+"</td>"
                        +"<td>"+i_cliente.getLatitud()+"</td>"
                        +"<td>"+i_cliente.getLongitud()+"</td>"
                        +"<td>"+i_cliente.getEstado()+"</td>"
                        + "</tr>";
                s_clientes=s_clientes+l_ventas_local;
            }
            s_clientes=s_clientes+"</tbody></table>";
            return s_clientes;
        } else {
            System.out.println("error al registrar cliente");
            return "<h4>Lista de reportes de clientes vacia</h4>";
        }
    }
    
    private String reporteVendedores(){
        ArrayList<Vendedores> l_vendores = m_Reportes.reporteVendedores();
        String s_vendedor="";
        if (l_vendores.size() > 0) {
            s_vendedor="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>nombre</th>" +
                        "<th>correo</th>" +
                        "<th>contrasena</th>" +
                        "<th>imagen</th>" +
                        "<th>celular</th>" +
                        "<th>latitud</th>" +
                        "<th>longitud</th>" +
                        "<th>estado</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Vendedores i_vendedor : l_vendores) {
                String l_ventas_local="<tr>"
                        +"<td>"+i_vendedor.getId()+"</td>"
                        +"<td>"+i_vendedor.getNombre()+"</td>"
                        +"<td>"+i_vendedor.getCorreo()+"</td>"
                        +"<td>"+i_vendedor.getContrasena()+"</td>"
                        +"<td>"+i_vendedor.getImagen()+"</td>"
                        +"<td>"+i_vendedor.getCelular()+"</td>"
                        +"<td>"+i_vendedor.getLatitud()+"</td>"
                        +"<td>"+i_vendedor.getLongitud()+"</td>"
                        +"<td>"+i_vendedor.getEstado()+"</td>"
                        + "</tr>";
                s_vendedor=s_vendedor+l_ventas_local;
            }
            s_vendedor=s_vendedor+"</tbody></table>";
            return s_vendedor;
        } else {
            System.out.println("error al registrar vendedor");
            return "<h4>Lista de reportes de vendedol_ventas vacia</h4>";
        }
    }
    
    private String reporteVentas(){
        
        ArrayList<Ventas> l_ventas=m_Reportes.reporteVentas();
        if (l_ventas.size()>0) {
            String s_reporteVenta="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>idcliente</th>" +
                        "<th>idvendedor</th>" +
                        "<th>total</th>" +
                        "<th>fecha</th>" +
                        "<th>hora</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Ventas i_venta : l_ventas) {
                String l_ventas_local="<tr>"
                        +"<td>"+i_venta.getId()+"</td>"
                        +"<td>"+i_venta.getIdCliente()+"</td>"
                        +"<td>"+i_venta.getIdVendedor()+"</td>"
                        +"<td>"+i_venta.getTotal()+"</td>"
                        +"<td>"+i_venta.getFecha()+"</td>"
                        +"<td>"+i_venta.getHora()+"</td>"
                        + "</tr>";
                s_reporteVenta=s_reporteVenta+l_ventas_local;
            }
            s_reporteVenta=s_reporteVenta+"</tbody></table>";
            return s_reporteVenta;
              
        }
        return "<h4>El reporte de ventas esta vacia</h4>";
 
    }
    
    private String reporteproductos(){
         ArrayList<Productos> l_Productos = m_Reportes.reporteProductos();
        String s_reporteVenta="";
        if (l_Productos.size() > 0) {
            s_reporteVenta="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>nombre</th>" +
                        "<th>descripcion</th>" +
                        "<th>precio</th>" +
                        "<th>tamaño</th>" +
                        "<th>imagen</th>" +
                        "<th>idtipo</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Productos re : l_Productos) {
                String l_ventas_local="<tr>"
                        +"<td>"+re.getId()+"</td>"
                        +"<td>"+re.getNombre()+"</td>"
                        +"<td>"+re.getDescripcion()+"</td>"
                        +"<td>"+re.getPrecio()+"</td>"
                        +"<td>"+re.getTamaño()+"</td>"
                        +"<td>"+re.getImagen()+"</td>"
                        +"<td>"+re.getTipo()+"</td>"
                        + "</tr>";
                s_reporteVenta=s_reporteVenta+l_ventas_local;
            }
            s_reporteVenta=s_reporteVenta+"</tbody></table>";
            return s_reporteVenta;
            //return "list producto  exitosamente";
        } else {
            System.out.println("error al registrar producto");
            return "<h4>Lista de reportes de productos esta vacia</h4>";
        }
    }
    
    private String reporteVentasOnline(){
        ArrayList<Ventas> l_ventas=m_Reportes.reporteVentasOnline();
        if (l_ventas.size()>0) {
            String s_reporteVenta="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>idcliente</th>" +
                        "<th>idvendedor</th>" +
                        "<th>total</th>" +
                        "<th>fecha</th>" +
                        "<th>hora</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Ventas i_venta : l_ventas) {
                String l_ventas_local="<tr>"
                        +"<td>"+i_venta.getId()+"</td>"
                        +"<td>"+i_venta.getIdCliente()+"</td>"
                        +"<td>"+i_venta.getIdVendedor()+"</td>"
                        +"<td>"+i_venta.getTotal()+"</td>"
                        +"<td>"+i_venta.getFecha()+"</td>"
                        +"<td>"+i_venta.getHora()+"</td>"
                        + "</tr>";
                s_reporteVenta=s_reporteVenta+l_ventas_local;
            }
            s_reporteVenta=s_reporteVenta+"</tbody></table>";
            return s_reporteVenta;
              
        }
        return "<h4>El reporte de ventas online esta vacia</h4>";
 
    }
            
}
