/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.TipoProductos;

/**
 *
 * @author tecno
 */
public class NTipoProductos {
    private TipoProductos m_TipoProducto;
    public NTipoProductos() {
        m_TipoProducto=new TipoProductos();
    }
    
    
    public String registrar(String[] p_parametros){
        if (validarParaRegistrar(p_parametros)) {
            m_TipoProducto.setNombre(p_parametros[0]);
            m_TipoProducto.setDescripcion(p_parametros[1]);
            m_TipoProducto.setImagen(p_parametros[2]);
            boolean res=m_TipoProducto.registrar();
            if (res) {
                return "registro de tipo de producto exitosamente!!!";
            }
            return "error en proceso de registro del tipo del producto";
        }
        return "Error los parametros no cumplen requisistos !!!";
    }
    
    public String eliminar(String[] p_parametros){
        if (validarParaEliminar(p_parametros)) {
            m_TipoProducto.setNombre(p_parametros[0]);
            boolean res=m_TipoProducto.eliminar();
            if (res) {
                return "eliminar de tipo de producto exitosamente!!!";
            }
            return "error en proceso de eliminacion del tipo del producto";
        }
        return "Error los parametros no cumplen requisistos !!!";
    }
    
    public String editar(String[] p_parametros){
        if (validarParaEditar(p_parametros)) {
            m_TipoProducto.setId(Integer.parseInt(p_parametros[0]));
            m_TipoProducto.setNombre(p_parametros[1]);
            m_TipoProducto.setDescripcion(p_parametros[2]);
            m_TipoProducto.setImagen(p_parametros[3]);
            boolean res=m_TipoProducto.registrar();
            if (res) {
                return "editar de tipo de producto exitosamente!!!";
            }
            return "error en proceso de edicion del tipo del producto";
        }
        return "Error los parametros no cumplen requisistos !!!";
    }
    
    public String listar(){
          ArrayList<TipoProductos> res = m_TipoProducto.listar();
        String res_tipoProducto="";
        if (res.size() > 0) {
            res_tipoProducto="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>nombre</th>" +
                        "<th>descripcion</th>" +
                        "<th>imagen</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (TipoProductos re : res) {
                String res_local="<tr>"
                        +"<td>"+re.getId()+"</td>"
                        +"<td>"+re.getNombre()+"</td>"
                        +"<td>"+re.getDescripcion()+"</td>"
                        +"<td>"+re.getImagen()+"</td>"
                        + "</tr>";
                res_tipoProducto=res_tipoProducto+res_local;
            }
            res_tipoProducto=res_tipoProducto+"</tbody></table>";
            return "<H2>lista de Tipo de productos</H2>"+res_tipoProducto;
            //return "list producto  exitosamente";
        } else {
            System.out.println("error al listar tipos producto");
            return "<h4>lista de Tipo productos vacia</h4>";
        }
            
    }

    private boolean validarParaRegistrar(String[] p_parametros) {
        if (p_parametros.length!=3) {
            return false;
        }
        
        
        return true;
    }

    private boolean validarParaEditar(String[] p_parametros) {
        if (p_parametros.length!=4) {
            return false;
        }
        if (!esNumero(p_parametros[0])) {
            return false;
        }
        return true;
    }

    private boolean validarParaEliminar(String[] p_parametros) {
        if (p_parametros.length!=1) {
            return false;
        }
        if (!esNumero(p_parametros[0])) {
            return false;
        }
        return true;
    }

    private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
