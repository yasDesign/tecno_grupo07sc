/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Vendedores;

/**
 *
 * @author tecno
 */
public class NVendedor {
    private Vendedores m_Vendedor;

    public NVendedor() {
        m_Vendedor = new Vendedores();
    }

    public String registrar(String[] p_parametros) {
        if (validarParaRegistrar(p_parametros)) {
            boolean res = false;
            m_Vendedor.setNombre(p_parametros[0]);
            m_Vendedor.setImagen(p_parametros[1]);
            m_Vendedor.setCorreo(p_parametros[2]);
            m_Vendedor.setContrasena(p_parametros[3]);
            m_Vendedor.setCelular(Integer.parseInt(p_parametros[4]));
            m_Vendedor.setDireccion(p_parametros[5]);
            m_Vendedor.setLatitud(Float.parseFloat(p_parametros[6]));
            m_Vendedor.setLongitud(Float.parseFloat(p_parametros[7]));

            res = m_Vendedor.registrar();
            if (res) {
                System.out.println("vendedor registrado exitosamente");
                return "vendedor registrado exitosamente";
            } else {
                System.out.println("error al registrar vendedor");
                return "error al registrar vendedor";
            }
        } else {
            System.out.println("ERROR: parametros no validos revisar la ayuda!!");
            return "ERROR: parametros no validos revisar la ayuda!!";
        }

    }

    public String eliminar(String[] p_parametros) {
        if (validarParaEliminar(p_parametros)) {
            boolean res = false;
            m_Vendedor.setId(Integer.parseInt(p_parametros[0]));
            res = m_Vendedor.eliminar();
            if (res) {
                System.out.println("vendedor eliminado exitosamente");
                return "vendedor eliminado exitosamente";
            } else {
                System.out.println("error al eliminar vendedor");
                return "error al eliminar vendedor";
            }
        } else {
            System.out.println("ERROR: parametros no validos revisar la ayuda!!");
            return "ERROR: parametros no validos revisar la ayuda!!";
        }

    }

    public String editar(String[] p_parametros) {
        if (validarParaEditar(p_parametros)) {
            boolean res = false;
            m_Vendedor.setId(Integer.parseInt(p_parametros[0]));
            m_Vendedor.setNombre(p_parametros[1]);
            m_Vendedor.setImagen(p_parametros[2]);
            m_Vendedor.setCorreo(p_parametros[3]);
            m_Vendedor.setContrasena(p_parametros[4]);
            m_Vendedor.setCelular(Integer.parseInt(p_parametros[5]));
            m_Vendedor.setDireccion(p_parametros[6]);
            m_Vendedor.setLatitud(Float.parseFloat(p_parametros[7]));
            m_Vendedor.setLongitud(Float.parseFloat(p_parametros[8]));
            res = m_Vendedor.editar();
            if (res) {
                System.out.println("vendedor editado exitosamente");
                return "vendedor editado exitosamente";
            } else {
                System.out.println("error al editar vendedor");
                return "error al editar vendedor";
            }
        } else {
            System.out.println("ERROR: parametros no validos revisar la ayuda!!");
            return "ERROR: parametros no validos revisar la ayuda!!";
        }
    }

    public String listar() {

        ArrayList<Vendedores> res = m_Vendedor.listar();
        String res_vendedor="";
        if (res.size() > 0) {
            res_vendedor="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>nombre</th>" +
                        "<th>correo</th>" +
                        "<th>contrasena</th>" +
                        "<th>imagen</th>" +
                        "<th>celular</th>" +
                        "<th>latitud</th>" +
                        "<th>longitud</th>" +
                        "<th>estado</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            for (Vendedores re : res) {
                String res_local="<tr>"
                        +"<td>"+re.getId()+"</td>"
                        +"<td>"+re.getNombre()+"</td>"
                        +"<td>"+re.getCorreo()+"</td>"
                        +"<td>"+re.getContrasena()+"</td>"
                        +"<td>"+re.getImagen()+"</td>"
                        +"<td>"+re.getCelular()+"</td>"
                        +"<td>"+re.getLatitud()+"</td>"
                        +"<td>"+re.getLongitud()+"</td>"
                        +"<td>"+re.getEstado()+"</td>"
                        + "</tr>";
                res_vendedor=res_vendedor+res_local;
            }
            res_vendedor=res_vendedor+"</tbody></table>";
            return "<h2>LISTA DE VENDEDORES</h2>"+res_vendedor;
            //return "list vendedor  exitosamente";
        } else {
            System.out.println("error al registrar vendedor");
            
            return "<div class='alert'> <strong>RESPUESTA!! </strong>Lista de venderores vacia</div>";
        }

    }

    private boolean validarParaRegistrar(String[] p_parametros) {
        ///validar cantidad de parametros
        int cantidad_parametrs = p_parametros.length;
        if (cantidad_parametrs != 8) {
            System.out.println("cantidad no validad");
            return false;
        }
        
        //validar parametros son correo
        if (!esCorreo(p_parametros[2])) {
            System.out.println("parametro no es correo");
            return false;
        }
        //validar parametros son numeros
        if (!esNumero(p_parametros[4])) {
            System.out.println("parametro no es numero");
            return false;
        }
        //validar parametros son flotantes
        if (!esFlotante(p_parametros[6]) || !esFlotante(p_parametros[7])) {
            System.out.println("parametros no son ubicacion");
            return false;
        }
        return true;
    }

    private boolean esCadena(String p_parametro) {
        return true;
    }

    private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean esFlotante(String s) {
        boolean isValid = true;
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException nfe) {
            isValid = false;
        }
        return isValid;
    }

    private boolean esCorreo(String p_parametro) {
        String emailPatron = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        Pattern pattern = Pattern.compile(emailPatron);
        Matcher matcher = pattern.matcher(p_parametro);
        if (matcher.matches()) {
            System.out.println("correo valido");
            return true;
        } else {
            System.out.println("correo no validao");
            return false;
        }

    }

    private boolean validarParaEditar(String[] p_parametros) {
        ///validar cantidad de parametros
        int cantidad_parametrs = p_parametros.length;
        if (cantidad_parametrs != 9) {
            System.out.println("cantidad no validad");
            return false;
        }
        
        //validar parametros son correo
        if (!esCorreo(p_parametros[3])) {
            System.out.println("parametro no es correo");
            return false;
        }
        //validar parametros son numeros
        if (!esNumero(p_parametros[5]) || !esNumero(p_parametros[0])) {
            System.out.println("parametro no es numero");
            return false;
        }
        return true;
    }

    private boolean validarParaEliminar(String[] p_parametros) {
        ///validar cantidad de parametros
        int cantidad_parametrs = p_parametros.length;
        if (cantidad_parametrs != 1) {
            System.out.println("cantidad no validad");
            return false;
        }
       
        //validar parametros son numeros
        if (!esNumero(p_parametros[0])) {
            System.out.println("parametro no es numero");
            return false;
        }
        return true;
    }
}
