/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Ventas;

/**
 *
 * @author tecno
 */
public class NVentas {
    private Ventas m_Ventas;

    public NVentas() {
        m_Ventas=new Ventas();
    }
    
    public String eliminar(String[] p_parametros){
        if (validarParaEliminar(p_parametros)) {
            m_Ventas.setId(Integer.parseInt(p_parametros[0]));
            boolean res=m_Ventas.eliminar();
            if(res){
                return "la venta se eliminado satisfactoriamente";
            }
            return "Error al intentar eliminar la venta";
        }
        return "No se pudo eliminar la venta verifica los parametros";
    }
    
    public String listar(){
        ArrayList<Ventas> l_ventas=m_Ventas.listar();
        if (l_ventas.size()>0) {
            String s_venta="<table style='margin-top:10px; border: 1px solid #717171; padding:0px; width:100%;'>"
                        +"<thead style='background-color:#008080; color:white;'>" +
                        "<tr><th>id</th>" +
                        "<th>idcliente</th>" +
                        "<th>idvendedor</th>" +
                        "<th>total</th>" +
                        "<th>fecha</th>" +
                        "<th>hora</th>" +
                        "</tr></thead>"
                    + "<tbody>";
            
            for (Ventas i_venta : l_ventas) {
                String res_local="<tr>"
                        +"<td>"+i_venta.getId()+"</td>"
                        +"<td>"+i_venta.getIdCliente()+"</td>"
                        +"<td>"+i_venta.getIdVendedor()+"</td>"
                        +"<td>"+i_venta.getTotal()+"</td>"
                        +"<td>"+i_venta.getFecha()+"</td>"
                        +"<td>"+i_venta.getHora()+"</td>"
                        + "</tr>";
                    s_venta=s_venta+res_local;
            }
            s_venta=s_venta+"</tbody></table>";
            return "<H2>lista de ventas</H2>"+s_venta;
        }
        return "<h4>la lista de ventas esta vacia</h4>";    
    }
    
    /**********************************************************************/
    /*********************VALIDACIONES**************************/
    /**********************************************************************/
    
    private boolean validarParaEliminar(String[] p_parametros) {
        if (p_parametros.length==1 && esNumero(p_parametros[0])) {
            return true;
        }
        return false;
    }

    private boolean esNumero(String p_parametro) {
        try {
            Integer.parseInt(p_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
}
